#!/bin/python3
import tensorflow as tf
import os
os.environ['KERAS_BACKEND'] = 'tensorflow'
import csv, random, argparse
from string import punctuation
import numpy as np
from numpy import array
from numpy import asarray
#from numpy import zeros
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
import time
import datetime
import pickle
from sklearn import svm

embedding_dim = 100
filter_sizes = [5]
num_filters = 128
l2_reg_lambda=  1
dropout_keep_prob =0.7
batch_size = 50
num_epochs = 10
evaluate_every =100
embed_size = 100
np.random.seed(14353)
n1 = 50
n2 = 100
n3 = 100
h1 = 5
h2 =5

gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)

sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))


def normalizing(x, axis):    
    norm = tf.sqrt(tf.reduce_sum(tf.square(x), axis=axis, keep_dims=True))
    normalized = x / (norm)   
    return normalized

def batch_iter(data, batch_size, num_epochs, shuffle=True):
    """
    Generates a batch iterator for a dataset.
    """
    data = np.array(data)
    data_size = len(data)
    num_batches_per_epoch = int((len(data)-1)/batch_size) + 1
    for epoch in range(num_epochs):
        # Shuffle the data at each epoch
        if shuffle:
            shuffle_indices = np.random.permutation(np.arange(data_size))
            shuffled_data = data[shuffle_indices]
        else:
            shuffled_data = data
        for batch_num in range(num_batches_per_epoch):
            start_index = batch_num * batch_size
            end_index = min((batch_num + 1) * batch_size, data_size)
            yield shuffled_data[start_index:end_index]


class TextCNN(object):

      def __init__(
      self, sequence_length, num_classes, vocab_size,
      embedding_size, filter_sizes, num_filters, l2_reg_lambda=0.0):

        # Placeholders for input, output and dropout
        self.input_x = tf.placeholder(tf.int32, [None, sequence_length], name="input_x")
        self.input_y = tf.placeholder(tf.float32, [None, num_classes], name="input_y")
        self.dropout_keep_prob = tf.placeholder(tf.float32, name="dropout_keep_prob")

        # Keeping track of l2 regularization loss (optional)
        l2_loss = tf.constant(0.0)

        # Embedding layer
        with tf.device('/GPU:0'), tf.name_scope("embedding"):
            self.W = tf.Variable(
                tf.random_uniform([vocab_size, embedding_size], -1.0, 1.0),
                name="W")
            self.W_norm = normalizing(self.W, 1)
            self.embedded_chars = tf.nn.embedding_lookup(self.W_norm, self.input_x)
            self.embedded_chars_expanded = tf.expand_dims(self.embedded_chars, -1)

        self.weights = {'ce1': tf.Variable(tf.truncated_normal([h1, embed_size, 1, n1],  stddev=0.1)),
                        'ce2': tf.Variable(tf.truncated_normal([h2, 1, n1, n2], stddev=0.1)),
                        'ce3': tf.Variable(tf.truncated_normal([sequence_length-h1-h2+2, 1, n2, n3], stddev=0.1)) }
        
        self.biases = { 'be1': tf.Variable(tf.constant(0.1, shape=[n1])),
                        'be2': tf.Variable(tf.constant(0.1, shape=[n2])),
                        'be3': tf.Variable(tf.constant(0.1, shape=[n3])),
                        'bd1': tf.Variable(tf.constant(0.1, shape=[1])),
                        'bd2': tf.Variable(tf.constant(0.1, shape=[n1])),
                        'bd3': tf.Variable(tf.constant(0.1, shape=[n2]))}

        #Encoder
        self.conv1 = tf.nn.conv2d(self.embedded_chars_expanded, self.weights['ce1'], strides=[1, 1, 1, 1], padding='VALID')
        self.ce1 = tf.nn.bias_add(self.conv1, self.biases['be1'])
        self.ce1 = tf.nn.dropout(self.ce1, self.dropout_keep_prob)

        self.conv2 = tf.nn.conv2d(self.ce1, self.weights['ce2'], strides=[1, 1, 1, 1], padding='VALID')
        self.ce2 = tf.nn.bias_add(self.conv2, self.biases['be2'])
        self.ce2 = tf.nn.dropout(self.ce2, self.dropout_keep_prob)

        self.conv3 = tf.nn.conv2d(self.ce2, self.weights['ce3'], strides=[1, 1, 1, 1], padding='VALID')
        self.ce3 = tf.nn.bias_add(self.conv3, self.biases['be3'])
        self.ce3 = tf.nn.dropout(self.ce3, self.dropout_keep_prob)
        #print(self.ce3)

        #classification using Hidden Representation
        self.h_pool_flat = tf.reshape(self.ce3, [-1, n3])

        # Add dropout
        with tf.name_scope("dropout"):
            self.h_drop = tf.nn.dropout(self.h_pool_flat, self.dropout_keep_prob)

        # Final (unnormalized) scores and predictions
        with tf.name_scope("output"):
            W = tf.get_variable(
                "W",
                shape=[n3, num_classes],
                initializer=tf.contrib.layers.xavier_initializer())
            b = tf.Variable(tf.constant(0.1, shape=[num_classes]), name="b")
            l2_loss += tf.nn.l2_loss(W)
            l2_loss += tf.nn.l2_loss(b)
            self.scores = tf.nn.xw_plus_b(self.h_drop, W, b, name="scores")
            self.predictions = tf.argmax(self.scores, 1, name="predictions")
            print(self.scores)

        # Calculate mean cross-entropy loss
        with tf.name_scope("loss"):
            losses = tf.nn.softmax_cross_entropy_with_logits(logits=self.scores, labels=self.input_y)
            self.classification_loss = tf.reduce_mean(losses) + l2_reg_lambda * l2_loss

        # Accuracy
        with tf.name_scope("accuracy"):
            correct_predictions = tf.equal(self.predictions, tf.argmax(self.input_y, 1))
            self.accuracy = tf.reduce_mean(tf.cast(correct_predictions, "float"), name="accuracy")

        #Decoder
        self.deconv3 = tf.nn.conv2d_transpose(self.ce3, self.weights['ce3'], tf.stack([tf.shape(self.embedded_chars_expanded)[0], sequence_length-h1-h2+2, 1, n2]), 
                                            strides=[1, 1, 1, 1], padding='VALID')
        self.de3 = tf.nn.bias_add(self.deconv3, self.biases['bd3'])
        self.de3 = tf.nn.dropout(self.de3, self.dropout_keep_prob)

        self.deconv2 = tf.nn.conv2d_transpose(self.de3, self.weights['ce2'], tf.stack([tf.shape(self.embedded_chars_expanded)[0], sequence_length-h1+1, 1, n1]), 
                                            strides=[1, 1, 1, 1], padding='VALID')
        self.de2 = tf.nn.bias_add(self.deconv2, self.biases['bd2'])
        self.de2 = tf.nn.dropout(self.de2, self.dropout_keep_prob)


        self.deconv1 = tf.nn.conv2d_transpose(self.de2, self.weights['ce1'], tf.stack([tf.shape(self.embedded_chars_expanded)[0], sequence_length, embed_size, 1]), 
                                            strides=[1, 1, 1, 1], padding='VALID')
        self.de1 = tf.nn.bias_add(self.deconv1, self.biases['bd1'])

        self.x_rec_norm = normalizing(self.de1, 2)
        
        prob_logits = tf.tensordot(tf.squeeze(self.x_rec_norm), self.W_norm, [[2],[1]])  # c_blv = sum_e x_ble W_ve
        prob = tf.nn.log_softmax(prob_logits*100, dim=-1, name=None)
        prob = tf.reshape(prob, [-1,vocab_size])
        idx = tf.range(batch_size * sequence_length)
        x_temp = tf.reshape(self.input_x, [-1,])
        all_idx = tf.transpose(tf.stack(values=[idx,x_temp]))
        all_prob = tf.gather_nd(prob, all_idx)
        self.reconstruction_loss = -tf.reduce_mean(all_prob)

        
        #self.reconstruction_loss = tf.reduce_mean(tf.square(self.x_rec_norm - self.embedded_chars_expanded ))

        self.loss = 0.3*self.reconstruction_loss + 0.7* self.classification_loss


def train_model(x_train, y_train, vocab_len, x_dev, y_dev):
    
    with tf.Graph().as_default():
        sess = tf.Session()
        with sess.as_default():
            cnn = TextCNN(
                sequence_length=x_train.shape[1],
                num_classes=y_train.shape[1],
                vocab_size = vocab_len,
                embedding_size = embedding_dim,
                filter_sizes= filter_sizes,
                num_filters= num_filters,
                l2_reg_lambda=l2_reg_lambda)

            # Define Training procedure
            global_step = tf.Variable(0, name="global_step", trainable=False)
            optimizer = tf.train.AdamOptimizer(1e-3)
            grads_and_vars = optimizer.compute_gradients(cnn.loss)
            train_op = optimizer.apply_gradients(grads_and_vars, global_step=global_step)

            #saver = tf.train.Saver(tf.global_variables(), max_to_keep=FLAGS.num_checkpoints)
            saver = tf.train.Saver()
            # Initialize all variables
            sess.run(tf.global_variables_initializer())
            sess.run(tf.local_variables_initializer())

            def train_step(x_batch, y_batch):
                feed_dict = {
                  cnn.input_x: x_batch,
                  cnn.input_y: y_batch,
                  cnn.dropout_keep_prob: dropout_keep_prob
                }
                _, step, classification_loss, reconstruction_loss, loss, accuracy = sess.run(
                    [train_op, global_step, cnn.classification_loss, cnn.reconstruction_loss, cnn.loss, cnn.accuracy],
                    feed_dict)
                time_str = datetime.datetime.now().isoformat()
                print("{}: step {}, class_loss {:g}, recons_loss {:g}, loss {:g}, acc {:g}".format(time_str, step, classification_loss, reconstruction_loss, loss, accuracy))
                

            def dev_step(x_dev, y_dev):
                dev_batches = batch_iter(list(zip(x_dev, y_dev)), batch_size, 1)
                avg_loss=0
                avg_acc = 0
                count = 0
                for batch in dev_batches:
                    x_batch, y_batch = zip(*batch)
                    feed_dict = { cnn.input_x: x_batch,cnn.input_y: y_batch,cnn.dropout_keep_prob: 1.0}
                    step,  loss, accuracy = sess.run( [global_step,  cnn.loss, cnn.accuracy],feed_dict)
                    avg_loss+=loss
                    avg_acc+=accuracy
                    count+=1

                avg_loss=avg_loss/float(count)
                avg_acc/=float(count)
                time_str = datetime.datetime.now().isoformat()
                print("{}: step {}, loss {:g}, acc {:g}".format(time_str, step, avg_loss, avg_acc))
                
            # Generate batches
            batches = batch_iter(
                list(zip(x_train, y_train)), batch_size, num_epochs)
            # Training loop. For each batch...
            for batch in batches:
                x_batch, y_batch = zip(*batch)
                train_step(x_batch, y_batch)
                current_step = tf.train.global_step(sess, global_step)
                if current_step % evaluate_every == 0:
                    print("\nEvaluation:")
                    dev_step(x_dev, y_dev)
                    print("")
            """
            feed_dict = {cnn.input_x: x_dev,cnn.input_y: y_dev,cnn.dropout_keep_prob: 1.0}
            print("Development Set Accuracy: ",sess.run(cnn.accuracy,feed_dict))
            """
            dev_step(x_dev, y_dev)

            save_path = saver.save(sess, "imdb_autoencoder/model.ckpt")
            print("Model saved in path: %s" % save_path)
   

class IMDBClassifier:
    def __init__(self):
        train_file = open('data/imdb-train.csv', 'r')
        self.train_reader = csv.reader(train_file)

        test_in_file = open('data/imdb-test.csv', 'r')
        self.test_reader = csv.reader(test_in_file)

        # Skip the header row
        next(self.train_reader)
        next(self.test_reader)

    def train(self):
        
        documents = list()
        labels = list()
        for i,entry in enumerate(self.train_reader):
            documents.append(entry[0].lower())
            if entry[1]=='pos':
                labels.append([0,1])
            else:
                labels.append([1,0])
            
        
        # create the tokenizer
        self.tokenizer = Tokenizer(oov_token='UNK',num_words = 10000)
        # fit the tokenizer on the documents
        self.tokenizer.fit_on_texts(documents)
 
        # sequence encode
        encoded_docs = self.tokenizer.texts_to_sequences(documents)
        # pad sequences
        self.max_length = 305
        X = pad_sequences(encoded_docs, maxlen= self.max_length, padding='post')
        y = np.array(labels)
        X_train, X_dev, y_train, y_dev = train_test_split(X, y, test_size=0.1, random_state=42)
        
        # define vocabulary size (largest integer value)
        vocab_size = len(self.tokenizer.word_index) + 1
        vocab_size = min(vocab_size, 10000)
        pickle.dump( self.tokenizer, open( "imdb_autoencoder-tokenizer.p", "wb" ) )
        pickle.dump( self.max_length, open( "imdb_autoencoder-max_length.p", "wb" ) )

        train_model(X_train, y_train, vocab_size, X_dev, y_dev)



    def predict(self):
        test_out_file = open('result/imdb-test-autoencoder.csv', 'w')
        self.test_writer = csv.writer(test_out_file, quoting=csv.QUOTE_ALL)

        self.test_writer.writerow(['review', 'rating'])

        with open('imdb_autoencoder-tokenizer.p', 'rb') as handle:
            tokenizer = pickle.load(handle)
        with open('imdb_autoencoder-max_length.p', 'rb') as handle:
            max_length = pickle.load(handle)

        vocab_size = min(len(tokenizer.word_index) + 1 , 10000)
        cnn = TextCNN(
                sequence_length=max_length,
                num_classes=2,
                vocab_size = vocab_size,
                embedding_size = embedding_dim,
                filter_sizes= filter_sizes,
                num_filters= num_filters,
                l2_reg_lambda=l2_reg_lambda)

        saver = tf.train.Saver()

        with tf.Session() as sess:
            saver = tf.train.import_meta_graph('imdb_autoencoder/model.ckpt.meta')
            saver.restore(sess,tf.train.latest_checkpoint('imdb_autoencoder/./'))

            for entry in self.test_reader:
                encoded_docs = tokenizer.texts_to_sequences([entry[0]])
                X = pad_sequences(encoded_docs, maxlen=max_length, padding='post')
                feed_dict = {cnn.input_x: X,cnn.dropout_keep_prob: 1.0}
                prediction = sess.run(cnn.predictions,feed_dict)
                #print(prediction[0])
                if prediction[0] == 0:
                    pred = 'neg'
                else:
                    pred = 'pos'
                #prediction = random.choice(['pos', 'neg'])
                #print(pred)
                self.test_writer.writerow([entry[0], pred])

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument('--train', action='store_true')
    parser.add_argument('--test', action='store_true')

    args = parser.parse_args()

    trainer = IMDBClassifier()
    
    if args.train:
        print('Training...')
        trainer.train()

    if args.test:
        print('Testing...')
        trainer.predict()
