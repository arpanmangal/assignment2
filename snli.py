#!/bin/python3
import tensorflow as tf
import os
os.environ['KERAS_BACKEND'] = 'tensorflow'
import csv, random, argparse
from string import punctuation
import numpy as np
from numpy import array
from numpy import asarray
#from numpy import zeros
from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from sklearn.model_selection import train_test_split
import time
import datetime
import pickle

embedding_dim = 100
embedding_size = 100
filter_sizes = [3,4,5]
num_filters = 128
l2_reg_lambda=  1
dropout_keep_prob =0.5
batch_size = 128
num_epochs = 20
evaluate_every =100

np.random.seed(14353)

gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=0.333)

sess = tf.Session(config=tf.ConfigProto(gpu_options=gpu_options))

def batch_iter(data, batch_size, num_epochs, shuffle=True):
    """
    Generates a batch iterator for a dataset.
    """
    data = np.array(data)
    data_size = len(data)
    num_batches_per_epoch = int((len(data)-1)/batch_size) + 1
    for epoch in range(num_epochs):
        # Shuffle the data at each epoch
        if shuffle:
            shuffle_indices = np.random.permutation(np.arange(data_size))
            shuffled_data = data[shuffle_indices]
        else:
            shuffled_data = data
        for batch_num in range(num_batches_per_epoch):
            start_index = batch_num * batch_size
            end_index = min((batch_num + 1) * batch_size, data_size)
            yield shuffled_data[start_index:end_index]

def cnn_layers(embedded_chars_expanded, sequence_length, num_filters, filter_sizes, reuse=False):

	pooled_outputs = []
	with tf.variable_scope('cnn_network', reuse=reuse):
		for i, filter_size in enumerate(filter_sizes):
			with tf.name_scope("conv-maxpool-%s" % filter_size):
				filter_shape = [filter_size, embedding_size, 1, num_filters]
				W = tf.Variable(tf.truncated_normal(filter_shape, stddev=0.1), name="W")
				b = tf.Variable(tf.constant(0.1, shape=[num_filters]), name="b")
				conv = tf.nn.conv2d(embedded_chars_expanded,W,strides=[1, 1, 1, 1],padding="VALID",name="conv")
				# Apply nonlinearity
				h = tf.nn.relu(tf.nn.bias_add(conv, b), name="relu")
				# Maxpooling over the outputs
				pooled = tf.nn.max_pool(h,ksize=[1, sequence_length - filter_size + 1, 1, 1],strides=[1, 1, 1, 1],padding='VALID',name="pool")
				pooled_outputs.append(pooled)

            
		num_filters_total = num_filters * len(filter_sizes)
		h_pool = tf.concat(pooled_outputs, 3)
		h_pool_flat = tf.reshape(h_pool, [-1, num_filters_total])

		# Add dropout
		with tf.name_scope("dropout"):
			h_drop = tf.nn.dropout(h_pool_flat, dropout_keep_prob)

	return h_drop

class TextCNN(object):

      def __init__(
      self, sequence_length, num_classes, vocab_size,
      embedding_size, filter_sizes, num_filters, l2_reg_lambda=0.0):

        # Placeholders for input, output and dropout
        self.input_x1 = tf.placeholder(tf.int32, [None, sequence_length], name="input_x1")
        self.input_x2 = tf.placeholder(tf.int32, [None, sequence_length], name="input_x2")

        self.input_y = tf.placeholder(tf.float32, [None, num_classes], name="input_y")
        self.dropout_keep_prob = tf.placeholder(tf.float32, name="dropout_keep_prob")

        # Keeping track of l2 regularization loss (optional)
        l2_loss = tf.constant(0.0)

        # Embedding layer
        with tf.device('/GPU:0'), tf.name_scope("embedding"):
            self.W = tf.Variable(
                tf.random_uniform([vocab_size, embedding_size], -1.0, 1.0),
                name="W")
            self.embedded_chars1 = tf.nn.embedding_lookup(self.W, self.input_x1)
            self.embedded_chars_expanded1 = tf.expand_dims(self.embedded_chars1, -1)
            self.embedded_chars2 = tf.nn.embedding_lookup(self.W, self.input_x2)
            self.embedded_chars_expanded2 = tf.expand_dims(self.embedded_chars2, -1)

        out1 = cnn_layers(self.embedded_chars_expanded1,
                          sequence_length,
                          num_filters=num_filters,
                          filter_sizes=filter_sizes)

        out2 = cnn_layers(self.embedded_chars_expanded2,
                          sequence_length,
                          num_filters=num_filters,
                          filter_sizes=filter_sizes,
                          reuse=True)

        self.rep = tf.concat([out1,out2],-1)
        num_filters_total = num_filters * len(filter_sizes)
        # Final (unnormalized) scores and predictions
        with tf.name_scope("output"):
            W = tf.get_variable(
                "W",
                shape=[2*num_filters_total, num_classes],
                initializer=tf.contrib.layers.xavier_initializer())
            b = tf.Variable(tf.constant(0.1, shape=[num_classes]), name="b")
            l2_loss += tf.nn.l2_loss(W)
            l2_loss += tf.nn.l2_loss(b)
            self.scores = tf.nn.xw_plus_b(self.rep, W, b, name="scores")
            self.predictions = tf.argmax(self.scores, 1, name="predictions")

        # Calculate mean cross-entropy loss
        with tf.name_scope("loss"):
            losses = tf.nn.softmax_cross_entropy_with_logits(logits=self.scores, labels=self.input_y)
            self.loss = tf.reduce_mean(losses) + l2_reg_lambda * l2_loss

        # Accuracy
        with tf.name_scope("accuracy"):
            correct_predictions = tf.equal(self.predictions, tf.argmax(self.input_y, 1))
            self.accuracy = tf.reduce_mean(tf.cast(correct_predictions, "float"), name="accuracy")
     	

def train_model(x1_train, x2_train, y_train, vocab_len, x1_dev, x2_dev, y_dev):
    
    with tf.Graph().as_default():
        sess = tf.Session()
        with sess.as_default():
            cnn = TextCNN(
                sequence_length=x1_train.shape[1],
                num_classes=y_train.shape[1],
                vocab_size = vocab_len,
                embedding_size = embedding_dim,
                filter_sizes= filter_sizes,
                num_filters= num_filters,
                l2_reg_lambda=l2_reg_lambda)

            # Define Training procedure
            global_step = tf.Variable(0, name="global_step", trainable=False)
            optimizer = tf.train.AdamOptimizer(1e-3)
            grads_and_vars = optimizer.compute_gradients(cnn.loss)
            train_op = optimizer.apply_gradients(grads_and_vars, global_step=global_step)

            #saver = tf.train.Saver(tf.global_variables(), max_to_keep=FLAGS.num_checkpoints)
            saver = tf.train.Saver()
            # Initialize all variables
            sess.run(tf.global_variables_initializer())

            def train_step(x1_batch, x2_batch, y_batch):
                feed_dict = {
                  cnn.input_x1: x1_batch,
                  cnn.input_x2: x2_batch,
                  cnn.input_y: y_batch,
                  cnn.dropout_keep_prob: dropout_keep_prob
                }
                _, step, loss, accuracy = sess.run(
                    [train_op, global_step, cnn.loss, cnn.accuracy],
                    feed_dict)
                time_str = datetime.datetime.now().isoformat()
                print("{}: step {}, loss {:g}, acc {:g}".format(time_str, step, loss, accuracy))
                

            def dev_step(x1_dev, x2_dev, y_dev):
            	dev_batches = batch_iter(list(zip(x1_dev,x2_dev ,y_dev)), batch_size, 1)
            	avg_loss=0
            	avg_acc = 0
            	count = 0
            	for batch in dev_batches:
            		x1_batch, x2_batch, y_batch = zip(*batch)
            		feed_dict = { cnn.input_x1: x1_batch,cnn.input_x2: x2_batch, cnn.input_y: y_batch,cnn.dropout_keep_prob: 1.0}
            		step,  loss, accuracy = sess.run( [global_step,  cnn.loss, cnn.accuracy],feed_dict)
            		avg_loss+=loss
            		avg_acc+=accuracy
            		count+=1

            	avg_loss=avg_loss/float(count)
            	avg_acc/=float(count)
            	time_str = datetime.datetime.now().isoformat()
            	print("{}: step {}, loss {:g}, acc {:g}".format(time_str, step, avg_loss, avg_acc))
                
            # Generate batches
            batches = batch_iter(
                list(zip(x1_train,x2_train, y_train)), batch_size, num_epochs)
            # Training loop. For each batch...
            for batch in batches:
                x1_batch, x2_batch, y_batch = zip(*batch)
                train_step(x1_batch,x2_batch, y_batch)
                current_step = tf.train.global_step(sess, global_step)
                if current_step % evaluate_every == 0:
                    print("\nEvaluation:")
                    dev_step(x1_dev, x2_dev, y_dev)
                    print("")
            
            dev_step(x1_dev, x2_dev, y_dev)
            
            save_path = saver.save(sess, "snli/model.ckpt")
            print("Model saved in path: %s" % save_path)
   

class SNLIClassifier:
	def __init__(self):
		train_file = open('data/snli-train.csv', 'r')
		self.train_reader = csv.reader(train_file)

		valid_file = open('data/snli-dev.csv', 'r')
		self.valid_reader = csv.reader(valid_file)

		test_in_file = open('data/snli-test.csv', 'r')
		self.test_reader = csv.reader(test_in_file)

		# Skip the header row
		next(self.train_reader)
		next(self.valid_reader)
		next(self.test_reader)

	def train(self):
		
		hypothesis = list()
		premises = list()
		labels = list()
		for i,entry in enumerate(self.train_reader):
			hypothesis.append(entry[0].lower())
			premises.append(entry[1].lower())
			if entry[2]=='entailment':
				labels.append([1,0,0])
			elif entry[2]=='contradiction':
				labels.append([0,1,0])
			else:
				labels.append([0,0,1])

		# create the tokenizer
		self.tokenizer = Tokenizer()
		# fit the tokenizer on the documents
		self.tokenizer.fit_on_texts(hypothesis+premises)
 
		# sequence encode
		encoded_hypothesis = self.tokenizer.texts_to_sequences(hypothesis)
		encoded_premises = self.tokenizer.texts_to_sequences(premises)
		# pad sequences
		l1 = max([len(s.split()) for s in hypothesis])
		l2 = max([len(s.split()) for s in premises])
		self.max_length = max(l1,l2)

		X1 = pad_sequences(encoded_hypothesis, maxlen=self.max_length, padding='post')
		X2 = pad_sequences(encoded_premises, maxlen=self.max_length, padding='post')
		y = np.array(labels)
		X1_train,X1_dev,X2_train,X2_dev, y_train, y_dev = train_test_split(X1, X2, y, test_size=0.1, random_state=42)

		# define vocabulary size (largest integer value)
		vocab_size = len(self.tokenizer.word_index) + 1

		pickle.dump( self.tokenizer, open( "snli-tokenizer.p", "wb" ) )
		pickle.dump( self.max_length, open( "snli-max_length.p", "wb" ) )

		train_model(X1_train, X2_train, y_train, vocab_size, X1_dev, X2_dev, y_dev)


	def predict(self):
		test_out_file = open('result/snli-test.csv', 'w')
		self.test_writer = csv.writer(test_out_file, quoting=csv.QUOTE_ALL)

		self.test_writer.writerow(['sentence1', 'sentence2', 'label'])

		with open('snli-tokenizer.p', 'rb') as handle:
			tokenizer = pickle.load(handle)
		with open('snli-max_length.p', 'rb') as handle:
			max_length = pickle.load(handle)

		cnn = TextCNN(
                sequence_length=max_length,
                num_classes=3,
                vocab_size = len(tokenizer.word_index) + 1,
                embedding_size = embedding_dim,
                filter_sizes= filter_sizes,
                num_filters= num_filters,
                l2_reg_lambda=l2_reg_lambda)

		saver = tf.train.Saver()

		with tf.Session() as sess:
			saver = tf.train.import_meta_graph('snli/model.ckpt.meta')
			saver.restore(sess,tf.train.latest_checkpoint('snli/./'))

			for entry in self.test_reader:
				encoded_hypo = tokenizer.texts_to_sequences([entry[0]])
				encoded_prem = tokenizer.texts_to_sequences([entry[1]])
				X1 = pad_sequences(encoded_hypo, maxlen=max_length, padding='post')
				X2 = pad_sequences(encoded_prem, maxlen=max_length, padding='post')
				feed_dict = {cnn.input_x1: X1,cnn.input_x2: X2,cnn.dropout_keep_prob: 1.0}
				prediction = sess.run(cnn.predictions,feed_dict)
				#print(prediction[0])
				if prediction[0] == 0:
					pred = 'entailment'
				elif prediction[0] == 1:
					pred = 'contradiction'
				else:
					pred = 'neutral'
				#prediction = random.choice(['pos', 'neg'])
				#print(pred)
				self.test_writer.writerow([entry[0], entry[1], pred])
		

if __name__ == "__main__":
	parser = argparse.ArgumentParser()
	parser.add_argument('--train', action='store_true')
	parser.add_argument('--test', action='store_true')

	args = parser.parse_args()

	trainer = SNLIClassifier()
	
	if args.train:
		print('Training...')
		trainer.train()

	if args.test:
		print('Testing...')
		trainer.predict()
